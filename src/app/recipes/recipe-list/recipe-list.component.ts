import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  @Output() recipeClicked: Recipe;
  @Output() selectedRecipe = new EventEmitter<Recipe>();

 recipes: Recipe[] = [
  new Recipe('Test Recipe', 'This is a simply test', 
  'https://pinchofyum.com/wp-content/uploads/Buffalo-Cauliflower-Tacos-with-Avocado-Crema-Recipe.jpg'),
  new Recipe('Test Recipe 2', 'This is a simply test 2', 
   'https://pinchofyum.com/wp-content/uploads/Buffalo-Cauliflower-Tacos-with-Avocado-Crema-Recipe.jpg'),
   new Recipe('Test Recipe 3', 'This is a simply test 3', 
   'https://pinchofyum.com/wp-content/uploads/Buffalo-Cauliflower-Tacos-with-Avocado-Crema-Recipe.jpg')
   
 ];

  constructor() { }

  ngOnInit() {
  }

  onRecipeClicked(recipe: Recipe){
    this.selectedRecipe.emit(recipe);
  }

}
