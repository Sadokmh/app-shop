export class Ingredient {
   
    constructor(public name: string, public amount:number){

    }

    //or it can be like this(normal)
        /* public name: string;
           public amount: number;

           constructor(name: string, num: number){
               this.name = name;
               this.amount = num;
               
    }
    */

}